-- Variables
QBCore = exports['qb-core']:GetCoreObject()
PlayerJob = {}
local onDuty = false
local DutyBlips = {}

-- Functions
local function CreateDutyBlips(playerId, playerLabel, playerJob, playerLocation)
    local ped = GetPlayerPed(playerId)
    local blip = GetBlipFromEntity(ped)
    if not DoesBlipExist(blip) then
        if NetworkIsPlayerActive(playerId) then
            blip = AddBlipForEntity(ped)
        else
            blip = AddBlipForCoord(playerLocation.x, playerLocation.y, playerLocation.z)
        end
        if IsPedInAnyPoliceVehicle(ped) and IsPedInAnyHeli(ped) then
            SetBlipSprite(blip, 43)
            ShowHeadingIndicatorOnBlip(blip, true)
            SetBlipRotation(blip, math.ceil(playerLocation.w))
            SetBlipScale(blip, 1.0)
            if playerJob == "police" then
                SetBlipColour(blip, 46)
            elseif playerJob == "lspd" then
                SetBlipColour(blip, 46)
            elseif playerJob == "bcso" then
                SetBlipColour(blip, 46)
            elseif playerJob == "lssd" then
                SetBlipColour(blip, 46)
            elseif playerJob == "sasp" then
                SetBlipColour(blip, 46)
            elseif playerJob == "sapr" then
                SetBlipColour(blip, 46)
            elseif playerJob == "doc" then
                SetBlipColour(blip, 46)
            elseif  playerJob == "ambulance" then
                SetBlipColour(blip, 1)
            elseif  playerJob == "doj" then
                SetBlipColour(blip, 40)
            end
            SetBlipAsShortRange(blip, true)
            BeginTextCommandSetBlipName('UPD')
            AddTextComponentString(playerLabel)
            EndTextCommandSetBlipName(blip)
            DutyBlips[#DutyBlips+1] = blip
                
        else
            SetBlipSprite(blip, 1)
            ShowHeadingIndicatorOnBlip(blip, true)
            SetBlipRotation(blip, math.ceil(playerLocation.w))
            SetBlipScale(blip, 1.0)
            if playerJob == "police" then
                SetBlipColour(blip, 38)
                
            elseif playerJob == "lspd" then
                SetBlipColour(blip, 38)
            elseif playerJob == "bcso" then
                SetBlipColour(blip, 46)
            elseif playerJob == "lssd" then
                SetBlipColour(blip, 46)
            elseif playerJob == "sasp" then
                SetBlipColour(blip, 26)
            elseif playerJob == "sapr" then
                SetBlipColour(blip, 69)
            elseif playerJob == "doc" then
                SetBlipColour(blip, 40)
            elseif  playerJob == "ambulance" then
                SetBlipColour(blip, 5)
            elseif  playerJob == "doj" then
                SetBlipColour(blip, 40)
            end
            SetBlipAsShortRange(blip, true)
            BeginTextCommandSetBlipName('UPD')
            AddTextComponentString(playerLabel)
            EndTextCommandSetBlipName(blip)
            DutyBlips[#DutyBlips+1] = blip
        end
    end

    if GetBlipFromEntity(PlayerPedId()) == blip then
        -- Ensure we remove our own blip.
        RemoveBlip(blip)
    end
end

-- Events
AddEventHandler('QBCore:Client:OnPlayerLoaded', function()
    local player = QBCore.Functions.GetPlayerData()
    PlayerJob = player.job
    onDuty = player.job.onduty
    TriggerServerEvent("posti-upd:client:UpdateBlips")

    if PlayerJob and PlayerJob.name ~= "police"then
        if DutyBlips then
            for _, v in pairs(DutyBlips) do
                RemoveBlip(v)
            end
        end
        DutyBlips = {}
    end
end)

RegisterNetEvent('QBCore:Client:OnPlayerUnload', function()
    TriggerServerEvent('posti-upd:client:UpdateBlips')
    if DutyBlips then
        for _, v in pairs(DutyBlips) do
            RemoveBlip(v)
        end
        DutyBlips = {}
    end
end)

RegisterNetEvent('QBCore:Client:OnJobUpdate', function(JobInfo)
  
    if JobInfo.name ~= "police" then
        if DutyBlips then
            for _, v in pairs(DutyBlips) do
                RemoveBlip(v)
            end
        end
        DutyBlips = {}
    end
    PlayerJob = JobInfo
    TriggerServerEvent("posti-upd:server:UpdateBlips")
end)


RegisterNetEvent('posti-upd:client:UpdateBlips', function(players)
    local player = QBCore.Functions.GetPlayerData()
    PlayerJob = player.job
    if PlayerJob and (PlayerJob.name == 'police' or PlayerJob.name == 'ambulance' or PlayerJob.name == 'doj') and
        PlayerJob.onduty then
        if DutyBlips then
            for _, v in pairs(DutyBlips) do
                RemoveBlip(v)
            end
        end
        DutyBlips = {}
        if players then
            for _, data in pairs(players) do
                local id = GetPlayerFromServerId(data.source)
                CreateDutyBlips(id, data.label, data.job, data.location)

            end
        end
    end
end)


