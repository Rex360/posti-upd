local QBCore = exports['qb-core']:GetCoreObject()

local isPolice = false
local player = QBCore.Functions.GetPlayerData()

local function Police()
    player = QBCore.Functions.GetPlayerData()
    PlayerJob = player.job

if PlayerJob.name == 'police' then isPolice = true else isPolice = false end
end


local function CheckPlate(NetworkID)
    if IsCheckingPlate then return end
    IsCheckingPlate = true
    local entity = NetworkGetEntityFromNetworkId(NetworkID)
    if DoesEntityExist(entity) then

        local plate = GetVehicleNumberPlateText(entity)
        QBCore.Functions.Notify("The vin number is " ..plate)

        IsCheckingPlate = false
        
    end
end


--Thread
CreateThread(function()
    Police()
    exports['qb-target']:AddGlobalVehicle({
        options = {
            {
                label = "Check Plate",
                icon = "fas fa-car-rear",
                action = function(entity)
                    CheckPlate(NetworkGetNetworkIdFromEntity(entity))
                end,
                canInteract = function(entity)
                    return isPolice and IsThisModelACar(GetEntityModel(entity))
                end
            }
        },
        distance = 2
    })
end)