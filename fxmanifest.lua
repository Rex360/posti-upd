fx_version 'cerulean'
game 'gta5'

name "Posti-UPD"
description "All posti UPD scripts"
author "Posti"
version "0.0.1"

shared_scripts {
	'shared/config.lua',
}

client_scripts {
	'client/*.lua'
}

server_scripts {
	'server/*.lua'
}

dependencies {
	'qb-core'
}
