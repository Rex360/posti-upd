local QBCore = exports['qb-core']:GetCoreObject()


--Items
QBCore.Functions.CreateUseableItem("panic_button", function(source, item)
    local xPlayer = QBCore.Functions.GetPlayer(source);
    if xPlayer.Functions.GetItemByName("panic_button") ~= nil then
        TriggerClientEvent("posti-upd:client:SendPoliceEmergencyAlert", source);
    end
end)

---BLIPS
local function UpdateBlips()
    local dutyPlayers = {}
    local players = QBCore.Functions.GetQBPlayers()
    for _, v in pairs(players) do
        if v and (v.PlayerData.job.name == "police" or v.PlayerData.job.name == "ambulance" or v.PlayerData.job.name == "doj") and v.PlayerData.job.onduty then
            local coords = GetEntityCoords(GetPlayerPed(v.PlayerData.source))
            local heading = GetEntityHeading(GetPlayerPed(v.PlayerData.source))
            dutyPlayers[#dutyPlayers+1] = {
                source = v.PlayerData.source,
                label = v.PlayerData.metadata["callsign"],
                job = v.PlayerData.job.name,
                location = {
                    x = coords.x,
                    y = coords.y,
                    z = coords.z,
                    w = heading
                }
            }
            -- print(v.PlayerData.source)
            -- print(v.PlayerData.metadata["callsign"])
            -- print(v.PlayerData.job.name)
            
        end
    end
    TriggerClientEvent("posti-upd:client:UpdateBlips", -1, dutyPlayers)
end

RegisterNetEvent('posti-upd:client:UpdateBlips', function()
    UpdateBlips()
end)



---Threads
--Blip thread
CreateThread(function()
    while true do
        Wait(5000)
        UpdateBlips()
    end
end)